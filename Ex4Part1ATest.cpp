#include "CaesarText.h"
#include <iostream>
#include <string>

using std::cout;
using std::endl;

std::string getShiftTextinfo(const ShiftText& s)
{
	return
		"[text: " + s.getText() +
		", encrypted: " + (s.isEncrypted() ? "true" : "false") + "]";
}

int test1Shift()
{

	try
	{
		// Tests Ex4 part 1 - ShiftText & CaesarText

		cout <<
			"********************\n" <<
			"Test 1 - ShiftText \n" <<
			"********************\n" << endl;

		cout <<
			"Initializing ShiftText object s1 with text = \"abcdefghijk\" , key = 10 ... \n" << endl;
		ShiftText s1("abcdefghijk", 10);

		std::string expected = "[text: klmnopqrstu, encrypted: true]";
		std::string got = getShiftTextinfo(s1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4101");
			std::cout << " \n" << endl;
			return 4101;
		}

		cout <<
			"\nCalling s1.decrypt() ... \n" << endl;

		s1.decrypt();
		expected = "[text: abcdefghijk, encrypted: false]";
		got = getShiftTextinfo(s1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4102");
			std::cout << " \n" << endl;
			return 4102;
		}

		cout <<
			"\nCalling s1.decrypt() again... \n" << endl;

		s1.decrypt();
		expected = "[text: abcdefghijk, encrypted: false]";
		got = getShiftTextinfo(s1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4103");
			std::cout << " \n" << endl;
			return 4103;
		}

		cout <<
			"\nCalling s1.encrypt() ... \n" << endl;

		s1.encrypt();
		expected = "[text: klmnopqrstu, encrypted: true]";
		got = getShiftTextinfo(s1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4104");
			std::cout << " \n" << endl;
			return 4104;
		}

		cout <<
			"\nCalling s1.encrypt() again... \n" << endl;

		s1.encrypt();
		expected = "[text: klmnopqrstu, encrypted: true]";
		got = getShiftTextinfo(s1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4105");
			std::cout << " \n" << endl;
			return 4105;
		}

		cout <<
			"\nInitializing ShiftText object s1 with text = \"lmnopqrstuvwxyz\" , key = 5 ... \n" << endl;
		ShiftText s2("lmnopqrstuvwxyz", 5);

		expected = "[text: qrstuvwxyzabcde, encrypted: true]";
		got = getShiftTextinfo(s2);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4101");
			std::cout << " \n" << endl;
			return 4101;
		}

		cout <<
			"\nCalling s2.decrypt() ... \n" << endl;

		s2.decrypt();
		expected = "[text: lmnopqrstuvwxyz, encrypted: false]";
		got = getShiftTextinfo(s2);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4102");
			std::cout << " \n" << endl;
			return 4102;
		}

		cout <<
			"\nInitializing CaesarText object c1 with text = \"abcdefghijklmnopqrstuvwxyz\" ... \n" << endl;
		CaesarText c1("abcdefghijklmnopqrstuvwxyz");

		expected = "[text: defghijklmnopqrstuvwxyzabc, encrypted: true]";
		got = getShiftTextinfo(c1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << std::endl;
			system("./printMessage 4101");
			std::cout << " \n" << endl;
			return 4106;
		}

		cout <<
			"\nCalling c1.decrypt() ... \n" << endl;

		c1.decrypt();
		expected = "[text: abcdefghijklmnopqrstuvwxyz, encrypted: false]";
		got = getShiftTextinfo(c1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4107");
			std::cout << " \n" << endl;
			return 4107;
		}

		cout <<
			"\nInitializing CaesarText object c2 with text = \"this is a string with comma, space and dots ...\" ... \n" << endl;
		CaesarText c2("this is a string with comma, space and dots ...");

		expected = "[text: wklv lv d vwulqj zlwk frppd, vsdfh dqg grwv ..., encrypted: true]";
		got = getShiftTextinfo(c2);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4108");
			std::cout << " \n" << endl;
			return 4108;
		}
		
		cout <<
			"\nCalling c2.decrypt() ... \n" << endl;

		c2.decrypt();
		expected = "[text: this is a string with comma, space and dots ..., encrypted: false]";
		got = getShiftTextinfo(c2);
		c1.decrypt();
		expected = "[text: abcdefghijklmnopqrstuvwxyz, encrypted: false]";
		got = getShiftTextinfo(c1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4109");
			std::cout << " \n" << endl;
			return 4109;
		}
	}
	catch (...)
	{
		std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
		std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return 2;
	}

	return 0;

}


int main()
{
	std::cout <<
		"###################################\n" <<
		"Exercise 4 - Encryption\n" <<
		"Part 1 - ShiftText & CaesarText\n" <<
		"###################################\n" << std::endl;

	int testResult = test1Shift();

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex4 Part 1 Tests Passed ******\033[0m\n \n" : "\033[1;31mEx4 Part 1 Tests Failed\033[0m\n \n") << endl;

	return testResult;
}