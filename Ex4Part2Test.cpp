#include "CaesarText.h"
#include "SubstitutionText.h"
#include <fstream>
#include <iostream>
#include <string>

using std::cout;
using std::endl;

void printSubDictionary(const std::string csvFileName)
{
	std::cout << "Printing \"" << csvFileName << "\"..." << std::endl;
	std::ifstream csv(csvFileName);
	std::string sourceLetter, destLetter, line;
	while (std::getline(csv, line))
	{
		sourceLetter = line[0];
		destLetter = line[2];
		cout << "" << sourceLetter << " ";
		cout << "<------> " << destLetter << std::endl;;
	}
}

int test3Static()
{

	try
	{
		// Tests Ex4 part 3 - Static

		cout <<
			"**********************************\n" <<
			"Test 3 - Static fields & methods \n" <<
			"**********************************\n" << endl;

		///////////////
		// ShiftText //
		///////////////

		cout <<
			"Checking ShiftText::encrypt ... \n" << endl;

		std::string text = "abcdefghijklmnopqrstuvwxyz";
		cout << "Text: \"" << text << "\" , key = 10" << endl;
		std::string expected = "klmnopqrstuvwxyzabcdefghij";
		std::string got = ShiftText::encrypt(text, 10);

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4301");
			std::cout << " \n" << endl;
			return 4301;
		}

		text = "abc, def, ghi, jkl, mno, pqr, stu, vwx, yz.";
		cout << "\nText: \"" << text << "\" , key = 5" << endl;
		expected = "fgh, ijk, lmn, opq, rst, uvw, xyz, abc, de.";
		got = ShiftText::encrypt(text, 5);

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4302");
			std::cout << " \n" << endl;
			return 4302;
		}

		cout <<
			"\nChecking ShiftText::decrypt ... \n" << endl;

		text = "klmnopqrstuvwxyzabcdefghij";
		cout << "Text: \"" << text << "\" , key = 10" << endl;
		expected = "abcdefghijklmnopqrstuvwxyz";
		got = ShiftText::decrypt(text, 10);

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4303");
			std::cout << " \n" << endl;
			return 4303;
		}

		text = "fgh, ijk, lmn, opq, rst, uvw, xyz, abc, de.";
		cout << "\nText: \"" << text << "\" , key = 5" << endl;
		expected = "abc, def, ghi, jkl, mno, pqr, stu, vwx, yz.";
		got = ShiftText::decrypt(text, 5);

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4304");
			std::cout << " \n" << endl;
			return 4304;
		}

		////////////////
		// CaesarText //
		////////////////

		cout << "\nChecking CaesarText::encrypt ... \n" << endl;

		text = "roses are red, my name is dave, this makes no sense, microwave.";
		cout << "Text: \"" << text << "\"" << endl;
		expected = "urvhv duh uhg, pb qdph lv gdyh, wklv pdnhv qr vhqvh, plfurzdyh.";
		got = CaesarText::encrypt(text);

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4305");
			std::cout << " \n" << endl;
			return 4305;
		}

		cout << "\nChecking CaesarText::decrypt ... \n" << endl;

		text = "urvhv duh uhg, pb qdph lv gdyh, wklv pdnhv qr vhqvh, plfurzdyh.";
		cout << "Text: \"" << text << "\"" << endl;
		expected = "roses are red, my name is dave, this makes no sense, microwave.";
		got = CaesarText::decrypt(text);

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4306");
			std::cout << " \n" << endl;
			return 4306;
		}

		//////////////////////
		// SubstitutionText //
		//////////////////////

		std::cout << "\n";
		const std::string dictFile = "dictionary.csv";

		cout << "\nChecking SubstitutionText::encrypt ..." << endl;

		text = "abc, def, ghi, jkl, mno, pqr, stu, vwx, yz.";
		cout << "\nText: \"" << text << "\" , key = " << dictFile << endl;
		expected = "rov, pxd, ncw, esm, bzi, jta, uqh, kfy, lg.";
		got = SubstitutionText::encrypt(text, dictFile);

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			printSubDictionary(dictFile);
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4307");
			std::cout << " \n" << endl;
			return 4307;
		}

		text = "sometimes when i close my eyes, i cant see.";
		cout << "\nText: \"" << text << "\" , key = " << dictFile << endl;
		expected = "uibxqwbxu fcxz w vmiux bl xlxu, w vrzq uxx.";
		got = SubstitutionText::encrypt(text, dictFile);

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			printSubDictionary(dictFile);
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4307");
			std::cout << " \n" << endl;
			return 4307;
		}

		cout << "\nChecking SubstitutionText::decrypt ..." << endl;

		text = "rov, pxd, ncw, esm, bzi, jta, uqh, kfy, lg.";
		cout << "\nText: \"" << text << "\" , key = " << dictFile << endl;
		expected = "abc, def, ghi, jkl, mno, pqr, stu, vwx, yz.";
		got = SubstitutionText::decrypt(text, dictFile);

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			printSubDictionary(dictFile);
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4308");
			std::cout << " \n" << endl;
			return 4308;
		}

		text = "uibxqwbxu fcxz w vmiux bl xlxu, w vrzq uxx.";
		cout << "\nText: \"" << text << "\" , key = " << dictFile << endl;
		expected = "sometimes when i close my eyes, i cant see.";
		got = SubstitutionText::decrypt(text, dictFile);

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			printSubDictionary(dictFile);
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4308");
			std::cout << " \n" << endl;
			return 4308;
		}

		/////////////////////////
		// Number of instances //
		/////////////////////////

		cout << "\nPrinting number of instances -- PlainText::numOfTexts " << std::endl;

		expected = "0";
		got = std::to_string(PlainText::numOfTexts);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4309");
			std::cout << " \n" << endl;
			return 4309;
		}

		cout << "\nInitializing 5 instances\n"
			<< "{PlainText p1, Shiftext s1, PlainText p2, CaesarText c1, SubstitutionText sb1}" 
			<< std::endl;

		PlainText p1("blabla");	
		ShiftText s1("Yalla yalla", 4);				
		PlainText p2("zoom zoom zoom");			
		CaesarText c1("abcdefghi");			
		SubstitutionText sb1("boo foo zoo", "dictionary.csv");	

		cout << "\nPrinting number of instances -- PlainText::numOfTexts " << std::endl;

		expected = "5";
		got = std::to_string(PlainText::numOfTexts);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4310");
			std::cout << " \n" << endl;
			return 4310;
		}
	}
	catch (...)
	{
		std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
		std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return 2;
	}

	return 0;

}


int main()
{
	std::cout <<
		"###################################\n" <<
		"Exercise 4 - Encryption\n" <<
		"Part 3 - Static\n" <<
		"###################################\n" << std::endl;

	int testResult = test3Static();

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex4 Part 2 Tests Passed ******\033[0m\n \n" : "\033[1;31mEx4 Part 2 Tests Failed\033[0m\n \n") << endl;

	return testResult;
}