#include "SubstitutionText.h"
#include <fstream>
#include <iostream>
#include <string>

using std::cout;
using std::endl;

std::string getSubTextinfo(const SubstitutionText& s)
{
	return
		"[text: " + s.getText() +
		", encrypted: " + (s.isEncrypted() ? "true" : "false") + "]";
}

void printSubDictionary(const std::string csvFileName)
{
	std::cout << "Printing \"" << csvFileName << "\"..." << std::endl;
	std::ifstream csv(csvFileName);
	std::string sourceLetter , destLetter, line;
	while (std::getline(csv, line)) 
	{
		sourceLetter = line[0];
		destLetter = line[2];
		cout << "" << sourceLetter << " ";
		cout << "<------> " << destLetter << std::endl;;
	}
}

int test2Sub()
{

	try
	{
		// Tests Ex4 part 2 - SubstitutionText

		cout <<
			"*****************************\n" <<
			"Test 2 - SubstitutionText \n" <<
			"*****************************\n" << endl;

		const std::string dictFile = "dictionary.csv";
		cout <<
			"\nInitializing SubstitutionText object s1 with text = \"abcdefghijk\" , key = " << dictFile << "... \n" << endl;
		SubstitutionText s1("abcdefghijk", dictFile);

		std::string expected = "[text: rovpxdncwes, encrypted: true]";
		std::string got = getSubTextinfo(s1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			printSubDictionary(dictFile);
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4201");
			std::cout << " \n" << endl;
			return 4201;
		}

		cout <<
			"\nCalling s1.decrypt() ... \n" << endl;

		s1.decrypt();
		expected = "[text: abcdefghijk, encrypted: false]";
		got = getSubTextinfo(s1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			printSubDictionary(dictFile);
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4202");
			std::cout << " \n" << endl;
			return 4202;
		}

		cout <<
			"\nCalling s1.decrypt() again ... \n" << endl;

		s1.decrypt();
		expected = "[text: abcdefghijk, encrypted: false]";
		got = getSubTextinfo(s1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			printSubDictionary(dictFile);
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4203");
			std::cout << " \n" << endl;
			return 4203;
		}

		cout <<
			"\nCalling s1.encrypt() ... \n" << endl;

		s1.encrypt();
		expected = "[text: rovpxdncwes, encrypted: true]";
		got = getSubTextinfo(s1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			printSubDictionary(dictFile);
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4204");
			std::cout << " \n" << endl;
			return 4204;
		}

		cout <<
			"\nCalling s1.encrypt() again... \n" << endl;

		s1.encrypt();
		expected = "[text: rovpxdncwes, encrypted: true]";
		got = getSubTextinfo(s1);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			printSubDictionary(dictFile);
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4205");
			std::cout << " \n" << endl;
			return 4205;
		}

		cout <<
			"\nInitializing SubstitutionText object s2 with text = \"this is a string with comma, space and dots ...\" , key = " 
			<< dictFile << "... \n" << endl;
		SubstitutionText s2("this is a string with comma, space and dots ...", dictFile);

		expected = "[text: qcwu wu r uqawzn fwqc vibbr, ujrvx rzp piqu ..., encrypted: true]";
		got = getSubTextinfo(s2);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			printSubDictionary(dictFile);
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4206");
			std::cout << " \n" << endl;
			return 4206;
		}


		cout <<
			"\nCalling s2.decrypt() ... \n" << endl;
		s2.decrypt();
		expected = "[text: this is a string with comma, space and dots ..., encrypted: false]";
		got = getSubTextinfo(s2);
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			printSubDictionary(dictFile);
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			system("./printMessage 4207");
			std::cout << " \n" << endl;
			return 4207;
		}
	}
	catch (...)
	{
		std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
		std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return 2;
	}

	return 0;

}


int main()
{
	std::cout <<
		"###################################\n" <<
		"Exercise 4 - Encryption\n" <<
		"Part 2 - SubstitutionText\n" <<
		"###################################\n" << std::endl;

	int testResult = test2Sub();

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex4 Part 1 Tests Passed ******\033[0m\n \n" : "\033[1;31mEx4 Part 1 Tests Failed\033[0m\n \n") << endl;

	return testResult;
}