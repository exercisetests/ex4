#include "FileHelper.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

using std::cout;
using std::endl;

std::string readFileToString(const std::string fileName)
{
	std::ifstream inFile;
	inFile.open(fileName); //open the input file

	std::stringstream strStream;
	strStream << inFile.rdbuf(); //read the file
	std::string str = strStream.str(); //str holds the content of the file

	return str;
}

bool test4Files()
{
	bool result = false;

	try
	{
		// Tests Ex4 part 4 - FileHelper

		cout <<
			"*********************\n" <<
			"Test 4 - FileHelper  \n" <<
			"*********************\n" << endl;

		cout <<
			"Checking FileHelper ... \n" << endl;

		const std::string file1name = "example.txt";
		std::string file1text = readFileToString(file1name);

		cout <<
			" \nCalling FileHelper::readFileToString(example.txt) ... \n" << endl;

		std::string expected = file1text;
		std::string got = FileHelper::readFileToString(file1name);

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected:\n" << expected << endl;
			cout << "\nGot     :\n" << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 4401");
			std::cout << " \n" << endl;
			return 4401;
		}

		const std::string outputFileName = "output.txt";
		cout <<
			"\nCalling FileHelper::saveTextInFile(file1Text, " << outputFileName << ") ... \n" << endl;

		FileHelper::saveTextInFile(file1text, outputFileName);

		cout <<
			"Comparing example.txt and " << outputFileName << " ... \n" << endl;

		expected = file1text;
		got = FileHelper::readFileToString(outputFileName);

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << "true" << endl;
			cout << "Got     : " << (got == expected ? "true" : "false") << endl;
			std::cout << " \n" << endl;
			system("./printMessage 4402");
			std::cout << " \n" << endl;
			return 4402;
		}

		const std::string output2FileName = "output2.txt";
		cout <<
			"\nCalling FileHelper::writeWordsToFile(" << file1name << ", " << output2FileName << ") ... \n" << endl;

		const std::string expectedResultFileName = "expected.txt";
		FileHelper::writeWordsToFile(file1name, output2FileName);

		expected = readFileToString(expectedResultFileName);
		got = FileHelper::readFileToString(output2FileName);

		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected:\n" << expected << endl;
			cout << "\nGot     :\n" << got << endl;
			system("./printMessage 4402");
			std::cout << " \n" << endl;
			return 4402;
		}

	}
	catch (...)
	{
		std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
		std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return 2;
	}

	return 0;

}


int main()
{
	std::cout <<
		"###################################\n" <<
		"Exercise 4 - Encryption\n" <<
		"Part 4 - Files\n" <<
		"###################################\n" << std::endl;

	int testResult = test4Files();

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex4 Part 3 Tests Passed ******\033[0m\n \n" : "\033[1;31mEx4 Part 3 Tests Failed\033[0m\n \n") << endl;

	return testResult;
}