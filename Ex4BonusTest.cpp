#include "BonusText.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>

using std::cout;
using std::endl;

std::string readFileToString(const std::string fileName)
{
	std::ifstream inFile;
	inFile.open(fileName); //open the input file

	std::stringstream strStream;
	strStream << inFile.rdbuf(); //read the file
	std::string str = strStream.str(); //str holds the content of the file

	return str;
}

int testBonus()
{

	try
	{
		// Tests Ex4 part 6 - Bonus

		cout <<
			"*****************\n" <<
			"Test 6 - Bonus	      \n" <<
			"*****************\n" << endl;

		cout <<
			"Checking BonusTest ... \n" << endl;

		const std::string encryptedFileName = "encrypted.txt";

		cout << "\nInitializing BonusText instances b1 with encrypted file name\n"
			 << "\ncalling b1.decrypt(encrypted.txt)\n"
			<< std::endl;

		BonusText b1(encryptedFileName);
		std::string text = b1.decrypt();

		std::string decryptedText = readFileToString("decrypted.txt");

		cout << "Expected:\n";
		cout << decryptedText << "\n" << endl;

		cout << "Got:\n"; 
		cout << text << endl;
		if (decryptedText != text)
			{
				cout << "FAILED: BonusText information is not as expected" <<
					"check function BonusText::decrypt()\n";
				return false;
			}
		
	}
	catch (...)
	{
		std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
		std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return 2;
	}

	return 0;

}


int main()
{
	std::cout <<
		"###################################\n" <<
		"Exercise 4 - Encryption\n" <<
		"Bonus \n" <<
		"###################################\n" << std::endl;

	int testResult = testBonus();

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex4 Bonus Tests Passed ******\033[0m\n \n" : "\033[1;31mEx4 Bonus Tests Failed\033[0m\n \n") << endl;

	return testResult;
}