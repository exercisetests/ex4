#include "CaesarText.h"
#include "SubstitutionText.h"
#include <iostream>
#include <string>

using std::cout;
using std::endl;

bool test5Operator()
{
	bool result = false;

	try
	{
		// Tests Ex4 part 5 - Operator <<

		cout <<
			"*********************\n" <<
			"Test 5 - Operator <<  \n" <<
			"*********************\n" << endl;

		cout <<
			"Checking Operator << ... \n" << endl;

		cout << "\nInitializing 5 instances\n"
			<< "{PlainText p1, Shiftext s1, PlainText p2, CaesarText c1, SubstitutionText sb1}" 
			<< std::endl;
			
		PlainText p1("blabla");	
		ShiftText s1("Yalla yalla", 4);				
		PlainText p2("zoom zoom zoom");			
		CaesarText c1("abcdefghi");			
		SubstitutionText sb1("boo foo zoo", "dictionary.csv");	
		
		std::string got, expected;

		cout << "\nUsing operator << on each instance " << std::endl;

		cout << "\nstd::cout << p1 << std::endl; " << std::endl;
		std::cout << p1 << std::endl;

		cout << "\nstd::cout << s1 << std::endl; " << std::endl;
		std::cout << s1 << std::endl;

		cout << "\nstd::cout << p2 << std::endl; " << std::endl;
		std::cout << p2 << std::endl;

		cout << "\nstd::cout << c1 << std::endl; " << std::endl;
		std::cout << c1 << std::endl;

		cout << "\nstd::cout << sb1 << std::endl; " << std::endl;
		std::cout << sb1 << std::endl;

	}
	catch (...)
	{
		std::cerr << "Test crashed" << endl;
		cout << "FAILED: The program crashed, check the following things:\n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return false;
	}

	cout << "\n########## Operator << - TEST Passed!!! ##########\n\n";

	return true;

}


int main()
{
	std::cout <<
		"###################################\n" <<
		"Exercise 4 - Encryption\n" <<
		"Part 5 - Operator << \n" <<
		"###################################\n" << std::endl;

	bool testResult = test5Operator();

	if (testResult)
	{
		std::cout << "\n########## Ex4 Operator << Test Passed!!! ##########" << "\n\n";
	}
	else
	{
		std::cout << "\n########## TEST Failed :( ##########\n";
	}
	return testResult ? 0 : 1;
}